sword-comm-tdavid (2.1-2) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Set upstream metadata fields: Repository.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 11 Jan 2023 02:50:16 -0000

sword-comm-tdavid (2.1-1) unstable; urgency=medium

  * New upstream version 2.1

 -- Bastian Germann <bage@debian.org>  Thu, 23 Jun 2022 21:15:00 +0200

sword-comm-tdavid (2.0-1) unstable; urgency=low

  * Team upload.
  * New upstream version 2.0.
  * Build from source (Closes: #984608)
  * d/copyright: Convert to Format 1.0
  * Address silent-on-rules-requiring-root
  * Remove debian/source/options

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Use secure URI in Homepage field.

 -- Bastian Germann <bage@debian.org>  Fri, 12 Nov 2021 22:40:54 +0100

sword-comm-tdavid (1.1.1-4) unstable; urgency=medium

  * dh_missing fail-missing

 -- Teus Benschop <teusjannette@gmail.com>  Thu, 01 Nov 2018 11:54:08 +0100

sword-comm-tdavid (1.1.1-3) unstable; urgency=high

  * debian/control: Multi-Arch: foreign

 -- Teus Benschop <teusjannette@gmail.com>  Mon, 22 Oct 2018 13:09:41 +0200

sword-comm-tdavid (1.1.1-2) unstable; urgency=medium

  * debian/control: Update uploaders

 -- Teus Benschop <teusjannette@gmail.com>  Tue, 16 Oct 2018 14:00:21 +0200

sword-comm-tdavid (1.1.1-1) unstable; urgency=medium

  * New upstream version 1.1.1

 -- Teus Benschop <teusjannette@gmail.com>  Fri, 05 Oct 2018 13:35:23 +0200

sword-comm-tdavid (1.1-8) unstable; urgency=medium

  * debian/control: Updated maintainer email: Closes: #899699
  * debian/control: Updated Vcs-* for Salsa

 -- Teus Benschop <teusjannette@gmail.com>  Fri, 05 Oct 2018 12:45:52 +0200

sword-comm-tdavid (1.1-7) unstable; urgency=medium

  * Set maintainer to CrossWire Packaging Team, move /me to uploaders
  * Updated to Standards-Version 4.1.2
  * Update debhelper dependency to >= 9 to match compat level

 -- Roberto C. Sanchez <roberto@debian.org>  Sat, 09 Dec 2017 23:40:30 -0500

sword-comm-tdavid (1.1-6) unstable; urgency=low

  * d/control: Bump Standards-Version to 3.9.8
  * d/control: Add Vcs-Git, Vcs-Browser
  * d/rules: convert to dh9
  * add debian/install to install files

 -- Dominique Corbex <dominique@corbex.org>  Sat, 27 May 2017 16:27:33 +0200

sword-comm-tdavid (1.1-5) unstable; urgency=low

  * Updated to Standards-Version 3.9.6 (no changes needed).
  * Add watch file documenting that upstream does not provide an index
    page.

 -- Roberto C. Sanchez <roberto@connexer.com>  Mon, 25 May 2015 15:19:21 -0400

sword-comm-tdavid (1.1-4) unstable; urgency=low

  * Updated to Standards-Version 3.9.4 (no changes needed).
  * Clean up some lintian warnings
  * Specify source format as 3.0 (quilt)

 -- Roberto C. Sanchez <roberto@connexer.com>  Sat, 03 Aug 2013 13:09:29 -0400

sword-comm-tdavid (1.1-3) unstable; urgency=low

  * Updated to Standards-Version 3.7.3 (no changes needed).
  * Update maintainer email address.
  * Updated to debhelper compatibility level 5.
  * debian/control: Switch Homepage to be a proper control field.

 -- Roberto C. Sanchez <roberto@connexer.com>  Sat,  5 Jan 2008 11:48:43 -0500

sword-comm-tdavid (1.1-2) unstable; urgency=low

  * Updated the debian/copyright file with instructions on how to extract the
    uncompressed text.

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Sun, 20 Nov 2005 15:52:43 -0500

sword-comm-tdavid (1.1-1) unstable; urgency=low

  * Initial Release.
  * Resolve ITP.  (closes: #338095)

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Mon,  7 Nov 2005 21:06:35 -0500
