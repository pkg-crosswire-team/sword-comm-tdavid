#!/bin/bash
rm -r tmp/ 

cp -r archive.spurgeon.org/treasury/ tmp
cd tmp
for FILE in *.html ; do
#id
sed -ri 's/(<TITLE>)/\1\\ms/g' $FILE
sed -ri 's/(honour and thanksgiving\.)(<BR>)/\1<\/I>\2/g;s/<I>(If it be remembered that the second and )/\1/g' $FILE
#v 1
sed -i 's/<\/i<\/FONT>><P><HR>/<\/I><\/FONT><P><HR>/g;s/<\/i><P><HR>/<\/I><P><HR>/g' $FILE
sed -ri 's/(<H1>|<h1>)(Psalm )([0-9]+)/\1\\c \3\\s \2\3\\p\\v 1/g' $FILE
sed -ri 's/<I>(Verse|Verses)<\/I>/\1/g;s/<B>(Verse [0-9]*\.|Verses [0-9]*, [0-9]*\.)<\/B>/\1/g;s/^(Verse [0-9]+\.|Verses [0-9]+\.|Verses [0-9]+, [0-9]+\.|Verses [0-9]*-[0-9]*\.)/\\p \\bd \1\\bd\*/g;s/&#151;/ —/g;s/(<A NAME="expo">|<A NAME="hint">)/\1\\s /g;s/<I>/<I>\\it /g;s/<\/I>/\\it\*<\/I>/g;s/<LI>/<LI>\\li /g' $FILE


#killall soffice.bin
#unoconv -f txt $FILE
unoconv -f txt $FILE
#html2text -o $FILE.txt $FILE

done
#rename 's/\.html//g' *txt
#sed -i '11,14d' *txt
sed -i '1,8d' *txt
sed -i '5,7d' *txt
rm treasury.txt
sed -i '/^$/d' *txt
sed -ri 's/^ *$//g' *txt
sed -ri 's/\[    \]/\\p /g;s/^ =*$//g;s/^( Exposition|DIVISION\.| *EXPOSITION|[A-Z ]*$)/\\s \1\n\\p/g;s/^   *([A-Z])/\\q \1/g;s/ \\it (Verse)\\it\*( [0-9]*.)/\\bd \1\2/g;s/ \**$//g;s/ \** (\\c)/\1/g' *txt
sed -ri 's/^([0-9]\. |\([a-z]\))/\\p \1/g' *txt
sed -ri 's/^([A-Z"][A-Za-z]*)/\\p \1/g' *txt
sed -i 's/^\\p Collection administered by Midwestern Baptist Theological Seminary\. Hosted by WPEngine\. For help and support, please email help@spurgeon\.org//g' *txt

cat *txt >Tdavid.txt
#sed -ri ':a;N;$!ba;s/(\\v [0-9]*)\n(\\p)/\2\n\1/g' *txt
#sed -i -e :a -e '$d;N;2,3ba' -e 'P;D' *txt
#sed -ri 's/@@/</g' *txt
#sed -ri 's/µµ/>/g' *txt

sed -ri 's/(\\c [0-9]+)(\\s .*)(\\p)(\\v 1)/\1\n\2\n\3\n\4/g' *txt
sed -i '1i\\\id PSA\n\\s ' Tdavid.txt
sed -ri 's/(y Preface)/\\p\nM\1/g' Tdavid.txt
sed -ri 's/^Other Work$//g' Tdavid.txt
sed -ri 's/(If it be remembered that the second|Preserve thou those that are appointed to die\."\\it)/\it \1/g' Tdavid.txt
sed -ri 's/([1-3]) (Jn|Chronicles|Samuel|Kings|Maccabees|John|Peter|Timothy|Thessalonians|Corinthians)( [0-9]*:)/\1\2\3/g' Tdavid.txt
sed -ri 's/^\\p Exposition$//g;s/(\\s Psalm [0-9]*)/\1 OVERVIEW/g' Tdavid.txt
sed -ri 's/^\\p( TITLE\.)( \\it)/\\s \1\n\\p\2/g' Tdavid.txt
cd ../
u2o.py -e utf-8 -l en -o tdavid.osis.xml -v -d tdavid -x tmp/Tdavid.txt
./Ref2Osis.sh
sed -ri 's/(Core\.)<reference osisRef="2John\.1\.1">2\.1\.1<\/reference>(\.xsd")/\12.1.1\2/g' tdavid.osis.xml.out
sed -ri 's/([1-3])(Jn|Chronicles|Pi|Samuel|Ti|Kings|Maccabees|John|Peter|Timothy|Thessalonians|Corinthians) /\1 \2 /g' tdavid.osis.xml.out
#xmllint --noout --schema ~/.bin/osisCore.2.1.1.xsd osis/clarke.osis.xml
mkdir ~/.sword/modules/comments/zcom/tdavidnv/
osis2mod ~/.sword/modules/comments/zcom/tdavidnv/ tdavid.osis.xml.out -s 4 -z

## Résoudre BÃ¶ch et RosenmÃ¼ller JehovÃ¦, æ
